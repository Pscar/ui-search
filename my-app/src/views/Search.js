import React from 'react';
import '../App.css';
import styled from 'styled-components';
import logo from '../image/Logo.png';
import { Row, Col, Image, Button } from 'antd';
import InputSeach from '../components/InputSeach';

const Search = () => {
  return (
    <main>
      <Row>
        <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
          <Login>
            <Button type="primary">Login</Button>
          </Login>
        </Col>
        <MainSearch>
          <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
            <Logo>
              <Image src={logo} alt="logo" preview={false} />
            </Logo>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
            <InputSeach />
          </Col>
        </MainSearch>
      </Row>
    </main>
  )
}
const MainSearch = styled.div`
  margin: 5% auto 0 auto;
`;
const Logo = styled.div`
	.ant-image {
		position: relative;
		display: grid;
	}
	.ant-image-img {
		margin-left: auto;
    margin-right: auto;
    width: 35%;
		margin-bottom:3rem;
	}
`;
const Login = styled.div`
	text-align:end;
	margin:2rem;
	.ant-btn-primary {
    border:none;
		border-radius: 12px;
		color: #fff;
		background: #001E7D;
		border-color: #1890ff;
	}
`;
export default Search
