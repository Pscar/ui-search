import React from 'react';
import { BrowserRouter } from "react-router-dom";
import './App.css';
import 'antd/dist/antd.css';
import Main from './components/Main';
import Footer from './components/Footer';
import Header from './components/Header';
const App = () => {
  return (
    <div className="container">
      <BrowserRouter>
        <Header />
        <Main />
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
