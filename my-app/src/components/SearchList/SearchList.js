import React from 'react'
import { Row, Col, Tabs, List } from 'antd';
import styled from 'styled-components';

const SearchList = () => {
	const { TabPane } = Tabs;

	function callback(key) {
		console.log(key);
	}
	const listData = [];
	for (let i = 0; i < 23; i++) {
		listData.push({
			href: 'https://ant.design',
			title: `ant design part ${i}`,
			avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
			description:
				'Ant Design, a design language for background applications, is refined by Ant UED Team.',
			content:
				'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
		});
	}
	return (
		<Row>
			<Col span={24}>
				<MainSearchList>
					<Tabs defaultActiveKey="1" onChange={callback}>
						<TabPane tab="Data" key="1">
						</TabPane>
						<TabPane tab="Visualization" key="2" disabled>
						</TabPane>
						<TabPane tab="Article" key="3" disabled>
						</TabPane>
					</Tabs>
				</MainSearchList>
			</Col>
			<Col span={24}>
				<MainListData>
					<List
						itemLayout="vertical"
						size="large"
						pagination={{
							onChange: page => {
								console.log(page);
							},
							pageSize: 5,
						}}
						dataSource={listData}
						renderItem={item => (
							<List.Item
								key={item.title}

							>
								<List.Item.Meta
									title={<a href={item.href}>{item.title}</a>}
									description={item.description}
								/>
								{item.content}
							</List.Item>
						)}
					/>
				</MainListData>
			</Col>
		</Row>
	)
}

const MainSearchList = styled.div`
	padding-left: 6%;
  padding-right: 6%;
`;
const MainListData = styled.div`
	padding-left: 5%;
  padding-right: 5%;
	.ant-list-pagination {
    margin-top: 24px;
    text-align: center;
	}
`;
export default SearchList
