import React from 'react';
import { Route } from "react-router-dom";

import '../App.css';
import DataExample from './SearchInfo/DataExample';
import Info from './SearchInfo/Info';

import SearchList from './SearchList/SearchList';

const Main = () => {
	return (
		<main>
			<Route path="/search-list" component={SearchList} />
			<Route path="/search-info" component={Info} />
			<Route path="/search-dataexample" component={DataExample}/>
		</main>
	)
}


export default Main
