import React from 'react'
import { Image, Row, Col } from 'antd';
import logo from '../image/logo_Landscape_white.png'
import styled from 'styled-components';

const Footer = () => {
  return (
    <footer>
      <MainFooter>
        <Row align="middle">
          <Col span={4}>
            <Image src={logo} alt="logo" preview={false} />
          </Col>
          <Col span={20}>
            <TextFooter>
              Address:  131 moo 9 INC1-217, Khlong Nueng,
              Khlong Luang District,  Pathum Thani  12120
              Telephone: 091-698-5205
              Email: contact@storemesh.com
              </TextFooter>
          </Col>
        </Row>
      </MainFooter>
    </footer>
  )
}

const MainFooter = styled.div`
.ant-image-img {
  display: grid;
  width: 100%;
  height: auto;
  margin-right: auto;
  margin-left: 80%;
}
`;
const TextFooter = styled.div`
  text-align: center;
  letter-spacing: 0px;
  color: #FFFFFF;
`;

export default Footer
