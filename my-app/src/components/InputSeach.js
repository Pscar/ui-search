import React from 'react'
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { Row, Col, Input, AutoComplete } from 'antd';


const renderTitle = (title) => (
	<span>
		{title}
		<a
			style={{
				float: 'right',
			}}
			href="https://www.google.com/search?q=antd"
			target="_blank"
			rel="noopener noreferrer"
		>
			more
    </a>
	</span>
);
const renderItem = (title, count) => ({
	value: title,
	label: (
		<div
			style={{
				display: 'flex',
				justifyContent: 'space-between',
			}}
		>
			{title}
			<span>
				{count}
			</span>
		</div>
	),
});

const options = [
	{
		label: renderTitle('Libraries'),
		options: [renderItem('AntDesign', 10000), renderItem('AntDesign UI', 10600)],
	},
	{
		label: renderTitle('Solutions'),
		options: [renderItem('AntDesign UI FAQ', 60100), renderItem('AntDesign FAQ', 30010)],
	},
	{
		label: renderTitle('Articles'),
		options: [renderItem('AntDesign design language', 100000)],
	},
];
const InputSeach = () => {
	return (
		<Row>
			<Col span={24}>
				<SearchInput>
					<AutoComplete
						dropdownMatchSelectWidth={500}
						options={options}
						placement="bottomCenter"
					>
						<Input size="large" suffix={<FontAwesomeIcon icon={faSearch} />} />
					</AutoComplete>
				</SearchInput>
			</Col>
		</Row>
	)
}
const SearchInput = styled.div`
	.ant-select {
		max-width: 100%;
		position: relative;
		display: grid;
		margin-left: auto;
		margin-right: auto;
	}
	.ant-input-affix-wrapper-lg {
		padding: 6.5px 11px;
		font-size: 16px;
		border-radius: 25px;
	}
	.ant-input-affix-wrapper > input.ant-input {
    border: none;
    outline: none;
    padding: 6.5px 11px;
    font-size: 16px;
	}
	.ant-input-suffix {
		padding: 6.5px 3px;
    color: #9aa0a6;
    margin-right: 4px;
	}
`;
export default InputSeach
