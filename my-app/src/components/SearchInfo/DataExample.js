import React from 'react'
import { Row, Col, Tabs, Table, PageHeader, Button } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileDownload, faChartLine } from '@fortawesome/free-solid-svg-icons'
import styled from 'styled-components';


const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}
const dataSource = [
  {
    name: 'Mike',
    detail: '10 Downing Street',
  },
  {
    name: 'John',
    detail: '10 Downing Street',
  },
];

const columns = [
  {
    title: 'province_no',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'province_name',
    dataIndex: 'detail',
    key: 'detail',
  },
];
const DataExample = () => {
  return (
    <Row>
      <Col span={24}>
        <MainPageHead>
          <PageHeader
            className="site-page-header"
            title="Title_Text"
            extra={[
              <Button key="1" type="primary" icon={<FontAwesomeIcon icon={faFileDownload} />}>Download</Button>,
              <Button key="2" type="primary" icon={<FontAwesomeIcon icon={faChartLine} />}>Visualization</Button>,
            ]}
          >
          </PageHeader>
        </MainPageHead>
      </Col>
      <Col span={24}>
        <MainDataEx>
          <Tabs defaultActiveKey="1" onChange={callback}>
            <TabPane tab="Info" key="1">
            </TabPane>
            <TabPane tab="Data Example" key="2" >
            </TabPane>
            <TabPane tab="Article" key="3" disabled>
            </TabPane>
          </Tabs>
        </MainDataEx>
      </Col>
      <Col span={24}>
        <MainTable>
          <Table dataSource={dataSource} columns={columns} />
        </MainTable>
      </Col>
    </Row>
  )
}

const MainDataEx = styled.div`
	padding-left: 6%;
  padding-right: 6%;
`;
const MainPageHead = styled.div`
	padding-left: 5%;
  padding-right: 5%;
	margin-top: 2%;
  margin-bottom: 2%;
	.ant-page-header-heading-title {
    margin-right: 12px;
    margin-bottom: 0;
    color: rgba(0, 0, 0, 0.85);
    font-weight: 600;
    font-size: 35px;
    line-height: 32px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
	}
	.ant-btn-primary {
    color: #fff;
		border-radius: 12px;
    background: #001E7D;
    border: none;
    text-shadow: 0 -1px 0 rgb(0 0 0 / 12%);
    -webkit-box-shadow: 0 2px 0 rgb(0 0 0 / 5%);
    box-shadow: 0 2px 0 rgb(0 0 0 / 5%);
	}
  .ant-btn > span {
    margin-left: 5%;
    display: inline-block;
  }
`;

const MainTable = styled.div`
	padding-left: 6%;
  padding-right: 6%;
`;
export default DataExample
