import React from 'react'
import '../App.css';
import logo from '../image/logo_Landscape.png';
import styled from 'styled-components';
import { Row, Col, Image, Button } from 'antd';
import InputSeach from './InputSeach';

const Header = () => {
	return (
		<nav>
			<MainHeader>
				<Row>
					<Col span={4}>
						<Image src={logo} alt="logo" preview={false} />
					</Col>
					<Col span={18}>
						<MainSeachInput>
							<InputSeach />
						</MainSeachInput>
					</Col>
					<Col span={2}>
						<Button type="primary">Login</Button>
					</Col>
				</Row>
			</MainHeader>
		</nav>
	)
}

const MainHeader = styled.div`
	.ant-image-img {
    margin-right: 1rem;
    margin-left: 1rem;
    display: grid;
    width: 80%;
    height: auto;
	}
	.ant-btn-primary {
		color: #fff;
		border-radius: 12px;
    background: #001E7D;
		border-color: #fff;
    width: 85%;
    margin-top: 0.3rem;
    margin-left: 20%;
    margin-right: 20%;
	}
	
`;
const MainSeachInput = styled.div`
	margin-left:auto;
	margin-right: auto;
	.ant-select {
    max-width: 100%;
    position: relative;
    display: grid;
	}
	.ant-input-affix-wrapper-lg {
		font-size: 16px;
		border-radius: 25px;
	}
	.ant-input-affix-wrapper > input.ant-input {
		margin-left: 0.9rem;
		padding: 0;
    font-size: 16px;
	}
`;
export default Header
